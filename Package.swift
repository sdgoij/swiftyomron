// swift-tools-version:5.5
import PackageDescription

let package = Package(
    name: "SwiftyOmron",
    products: [
        .library(
            name: "OmronConnectivityLibrary",
            targets: ["OmronConnectivityLibrary",]),
    ],
    dependencies: [],
    targets: [
        .binaryTarget(
            name: "OmronConnectivityLibrary",
            path: "Frameworks/OmronConnectivityLibrary.xcframework"
        )
    ]
)
