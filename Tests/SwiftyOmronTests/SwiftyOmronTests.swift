import XCTest
@testable import SwiftyOmron

final class SwiftyOmronTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(SwiftyOmron().text, "Hello, World!")
    }
}
